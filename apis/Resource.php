<?php

namespace App\Apis;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Guzzle\Http\Exception\BadResponseException;
use DI\Container;

class Resource
{
	/**
     * GuzzleHttp Client instance
     *
     * @var object $client
     */
	private $client;

	/**
     * Api url
     *
     * @var string $url
     */
	private $url;

	/**
     * Api credentials
     *
     * @var array $credentials
     */
	private $credentials;

	/**
     * DI Container
     *
     * @var object $c
     */
	private $c;

	public function __construct(Client $client, Container $c)
	{
		$this->client = $client;
		$this->url = $_SERVER['RESOURCE_URL'];
		$this->credentials = [
			$_SERVER['RESOURCE_USERNAME'], $_SERVER['RESOURCE_PASS']
		];
		$this->c = $c;
	}

	public function list(bool $cache): array
	{
		if($cache){
			# if pagination show from cache
			return json_decode($this->c->get('cache')->retrieve('list'), true) ?? [];
		}

		try {

		    $res = $this->client->request('get', $this->url .'/list', [
				'auth' => $this->credentials
			]);

		}catch (RequestException $e) {
		    return json_decode($this->c->get('cache')->retrieve('list'), true) ?? [];
		}

		if($res->getStatusCode() != 200){
			return json_decode($this->c->get('cache')->retrieve('list'), true) ?? [];
		}

		if($data = $res->getBody()->getContents()){
			$this->c->get('cache')->store("list", $data, 100);
		}

		return json_decode($data, true);
	}
}