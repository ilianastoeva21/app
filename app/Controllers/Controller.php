<?php

namespace App\Controllers;

use DI\Container;

class Controller
{
	protected $c;

	public function __construct(Container $container)
	{
		$this->c = $container;
	}

	public function __get($property)
    {
        if ($this->c->get($property)) {
            return $this->c->get($property);
        }
    }
}