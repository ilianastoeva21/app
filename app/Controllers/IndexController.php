<?php

namespace App\Controllers;

class IndexController  extends Controller
{
	public function index($request, $response)
	{
		$params = $request->getQueryParams();

		$page      = ($params['page'] > 0) ? $params['page'] : 1;
        $limit     = 20;
        $skip      = ($page - 1) * $limit;

		$res = $this->resource->list($page > 1 ?? true);
		$count = count($res) ?? 1000;
		$lastpage = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));

		return $this->c->get('view')->render($response, 'index.twig',[
			'results' => array_slice( $res, $skip, $limit ),
			'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit
            ]
		]);
	}
}