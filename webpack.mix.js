const mix = require('laravel-mix');

mix.autoload({
  jquery: ['$', 'jQuery', 'window.jQuery']
});

mix.js('resources/js/app.js', 'dist')
   .sass('resources/sass/app.scss', 'dist');