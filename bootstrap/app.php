<?php

use DI\Container;
use Dotenv\Dotenv;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use GuzzleHttp\Client;
use App\Apis\Resource;
use Wruczek\PhpFileCache\PhpFileCache;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();

// Create Container using PHP-DI
$container = new Container();
AppFactory::setContainer($container);

// settings
$container->set('settings', function(){
	return [
		'app' => [
			'name' => $_SERVER['APP_NAME'],
			'url' => $_SERVER['APP_URL']
		]
	];
});

// Set view in Container
$container->set('view', function() {
    
    $view = Twig::create(__DIR__ . '/../resources/views');

    $view->getEnvironment()->addGlobal("app", $container['settings']);

    return $view;
});

$container->set('cache', function() {
    return new PhpFileCache(__DIR__ . "/../cache/");
});

$container->set('resource', function ($container) {
    return new Resource(new Client(), $container);             
});


$app = AppFactory::create();

// Add Routing Middleware
$app->addRoutingMiddleware();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// Add Twig-View Middleware
$app->add(TwigMiddleware::createFromContainer($app));


require __DIR__ . '/../routes/web.php';